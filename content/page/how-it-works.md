---
title: How it works
comments: false
layout: article
aside:
  toc: true
---

# Hardware concept

As a baseline, the overall design for the Adaptive Optics (AO) Real-Time Controller (RTC)  consists of:
-	a Hard-real-time controller (HRTC): a high throughput, low latency and low jitter engine, taking as input pixel data from multiple cameras and providing as output the drive data for thousands of actuators
-	a Soft-real-time cluster (SRTC): a computing facility for the configuration, calibration and optimization of the hard-real-time pipeline (supervisor) and for data telemetry and storage (telemetry)
-	a Simulator system: providing accurate simulation at real-time frame rates of hardware components in their absence (emulation mode), as well as a quasi real-time end-to-end numerical simulator to validate achievable AO performance
-	A Communication Infrastructure: realizing all the required interconnections between these sub-systems.

These functional blocks are assembled together following the block diagram below

![AO_RTC_subsystem](/img/AO_RTC_subsystem.png)

A dedicated Hardware Interface Unit is used to provide the critical flexibility that is required to interconnect the hardware on the optical bench with the compute units.
The design of the HRTC module is driven by the ability to leverage GPU performance in real-time with streaming data from sensors. To achieve deterministic performance, routing efficiently these streaming data to the GPU memory is critical. Several solutions are being implemented for this interface including the use of [FPGA boards](https://greenflash-h2020.eu/-smart-interconnects-.html) as well as [smart network controllers](https://insu.hal.science/insu-04041377).

# Software stack

The COSMIC software is based on abstraction layers, providing a stable API for top-level scripts while allowing application specific implementations. Real-time pipelines as built by COSMIC are using a multi-process approach. A pipeline can be seen as a multi-process graph where a node is an independent process with task and/or data dependencies between nodes.
This approach allows high modularity and robustness, as a node can be replaced by another one on-the-fly. Inter-Process Communication (IPC) is based on shared memory which can be located either on the host side through Unix shared memory, or on the GPU side using CUDA IPC. Inter-process synchronization is implemented using POSIX semaphores for CPU-based algorithms, and using a GPU busy-wait approach for GPU data driven synchronization.

![COSMIC_Software_stack](/img/COSMIC_Software_stack.png)

OCEAN, the Optimized Core Engine for Adaptive optics pipeliNes, is a flexible implementation of the HRTC software, designed to be executed in an heterogeneous environment, including multiple threads on a CPU and thousands of computational threads on several GPUs. It includes several subcomponents as described in the Figure below.

![Kraken_description](/img/Kraken_description.png)

Kraken is a Python based software that aims at managing the RTC pipeline. It is also the user interface with the RTC. From a set of parameter files given by the user, Kraken is in charge of:
-	Loading the configuration from the file
-	Populating a redis database from this configuration
-	Allocating and managing the shared memory needed by the pipeline
-	Launching each configuration daemon and business unit processes
-	Monitoring the pipeline

![KrakenManager_description](/img/KrakenManager_description.png)

The SRTC software is composed of two main components:
-	SHIPS: Smart and HIgh Performance Supervisor, a software stack gathering the sequencer, the user interface, services such as alarms and logs handling as well as compute intensive modules for supervising and optimizing the AO loop. It is still under heavy development and we plan to leverage the extended experience of the COSMIC partners in designing, implementing and operating AO systems to provide a feature-rich software stack covering the needs of most AO concepts
-	TIDES: Telemetry Interface and Data Exchange Software, is a bundle covering the middleware domains for sending commands to the RTC and publishing / receiving telemetry data. It is based on abstraction layers for the main functions in order to make the code base agnostic to any particular software standards.

Finally, COMPASS (COMputing Platform of Adaptive opticS System) is an end-to-end simulation tool for AO systems able to address the largest scales. It takes advantage of GPU acceleration using the Nvidia CUDA toolkit. A comprehensive software stack has been designed to provide at the same time, both high performance computing, brought by massively parallel algorithms running on GPU and ease of use, brought by a user interface based on python and a graphics toolkit. More details available [here](https://compass.pages.obspm.fr/website/).

---
title: COSMIC Team
comments: false
layout: article
aside:
  toc: true
---

{{% module image_path="/img/headshot_gratadour.png" title="Damien Gratadour" width="150" description="**COSMIC lead**, astronomer and senior research scientist at Observatoire de Paris - CNRS. As a technology enthousiast, Damien  has successfully spearheaded numerous disruptive R&D projects in astronomical instrumentation for over a decade, including COSMIC. " %}}

{{% module image_path="/img/headshot_ferreira.jpg" title="Florian Ferreira" width="150" description="COSMIC guru and main software architect. As a software reasearch engineer (PhD) at Observatoire de Paris - CNRS, Florian is managing the effort for the design and manufacturing of SPHERE+ RTC. He is also the COMPASS lead and main architect." %}}

{{% module image_path="/img/headshot_sevin.jpg" title="Arnaud Sevin" width="150" description="COSMIC guru and core contributor. As a software reasearch engineer at Observatoire de Paris - CNRS, Arnaud is managing the effort for the design and manufacturing of MICADO RTC. As a mercenary technologist, he is also an expert in network architectures, middleware and high performance computing." %}}

{{% module image_path="/img/headshot-jbernard.jpg" title="Julien Bernard" width="150" description="COSMIC guru and core contributor. As a software reasearch engineer at AITC - ANU, Julien has been the core developer for GHOST RTC and is responsible for the MAVIS HRTC development. As a C++ guru, Julien is always pushing for integration of new features and tools into COSMIC, with a vision." %}}

{{% module image_path="/img/headshoot-tux.png" title="Nicolas Doucet" width="150" description="COSMIC core contributor. As a software reasearch engineer (PhD) at AITC - ANU now back to Observatoire de Paris - CNRS, Nicolas has been responsible for the MAVIS SRTC development and specializes in handling large HPC workflows for AO loop supervision including new highly optimized linear algebra." %}}

{{% module image_path="/img/jplante-headshot.jpg" title="Julien Plante" width="150" description="COSMIC core contributor. As a PhD student and transient radio-astronomy expert, Julien has been contributing to the smart NIC integration in COSMIC in collaboration with Nvidia and responsible for prototyping on NenuFAR." %}}

{{% module image_path="/img/ccetre-headshot.jpg" title="Cyril Cetre" width="150" description="COSMIC core contributor. As a PhD student with Thales Research & Technology, Cyril has been developing and validating new features in COSMIC fostering portability across compute hardware options, including a new multithread pipeline for operations on embedded platforms." %}}


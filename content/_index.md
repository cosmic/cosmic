## What is COSMIC ?
COSMIC (COmmon Scalable and Modular Infrastructure for real-time Control) is a Real-Time Computing platform for applications in astronomical instrumentation, maximizing efficiency and minimizing cost and complexity, with long term maintainability and scalability as the core drivers of the development cycle. 

The hardware design has emerged from a trade-off analysis of current and future trends in High Performance Computing (HPC), which minimizes the risk for obsolescence while maximizing performance. The software bundle relies on a comprehensive, scalable and highly modular architecture that includes hard real-time capabilities coupled to HPC implementations and supervisory software.

COSMIC is already integrated or chosen as the baseline for several Adaptive Optics (AO) facility systems:
- Keck telescope
- MICADO for ELT
- MAVIS for VLT
- SPHERE+ for VLT

## Hardware implentation
The COSMIC hardware impentation relies on dense clusters of GPUs and many-cores, coupled with smart network controllers, to get the best performance and reach hard real-time capabilities. While the GPUs provides high computing throughput and memory bandwidth, smart NICs enable low latency data transport from sensors and to actuators.

On top of the hard real-time controller, a supervision stage can either be added on the same cluster, providing an all-in-one solution, or be distributed across multiple nodes. This stage is in charge of all the supervision tasks for the hard real-time pipeline. It includes optimization processes, telemetry, monitoring, etc... Depending on the optimization workload, this stage can also include a dense GPU cluster, useful for tomography or AI training for instance.

## Software stack
The software platform relies as much as possible on off-the-shelf standard libraries and a both optimised and modular approach to build a variety of pipelines able to cope with various AO flavors, from single conjugate to multi-conjugate and multi-object AO as well as other kinds of cyber-physical systems. The comprehensive software stack includes the hard real-time pipeline as well as a supervisory software and a middleware solution, based on abstraction layers to adapt to a variety of frameworks and settings. 

## COSMIC development team
COSMIC development and long-term support relies on a stategic partnership between Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique ([LESIA](https://lesia.obspm.fr/)) at Observatoire de Paris, France, and the Advanced Instrumentation Technology Center ([AITC](https://rsaa.anu.edu.au/aitc)), part of the Research School of Astronomy and Astrophysics at the Australian National University (ANU), together with a close collaboration with [Microgate](https://www.microgate.it/en/).





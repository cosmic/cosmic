---
title: Keck RTC
bigimg: [{src: "../../img/keck-banner.jpg",  desc: "Keck Observatory, Credits: Julie Thurston (www.juliethurstonphotography.com)"}]
---

[W. M. Keck Observatory](https://www.keckobservatory.org/) has integrated a new real-time controller (NRTC) in support of the [KAPA](https://keckao.github.io/kapa/) project on the Keck I telescope. A second, identical NRTC system has been installed with the Keck II AO system. Unique aspects of the Keck II system include a SAPHIRA-detector based near-infrared pyramid wavefront sensor and a MEMS deformable mirror. A Consortium led by Microgate has designed and built the NRTC, based on COSMIC, to support existing and future AO capabilities for the Keck AO facilities. 

The hardware architecture with the use of GPUs allows for future changes and greater processing capacity while the software architecture provides the flexibility needed to test new algorithms and to integrate new sensors and controls. The system itself has greater reliability and has built-in tools to support failure isolation within the system. The NRTC has demonstrated its bandwidth performance in single Laser and Natural Guide Star (LGS and NGS) modes and is scheduled for an LTAO upgrade using four laser guide stars. This upgrade will demonstrate one of its main requirements in terms of being flexible to support new algorithms and hardware. 

A press release is available [here](https://www.bigislandvideonews.com/2020/09/30/kecks-upgraded-adaptive-optics-system-a-first-for-science/), mentioning Sylvain Cetre, a software engineer at Keck Observatory and one of the lead project developers. *“The new RTC performs heavy computations in the shortest time possible at very high speeds, resulting in a dramatic improvement in image quality.”*


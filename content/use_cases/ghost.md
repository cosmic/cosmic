---
title: GHOST RTC
bigimg: [{src: "../../img/ghost-banner-new3.png" , desc: "GHOST bench at ESO, Credits: Byron Engler, Markus Kasper (ESO)"}]
---

The GPU-based High-order adaptive OpticS Testbench ([GHOST](https://www-spiedigitallibrary-org.ezproxy.obspm.fr/conference-proceedings-of-spie/12185/1218558/The-GPU-based-High-order-adaptive-OpticS-Testbench/10.1117/12.2630595.full?SSO=1)) at the European Southern Observatory (ESO) is a new 2-stage extreme adaptive optics (XAO) testbench at ESO. The GHOST is designed to investigate and evaluate new control methods (machine learning, predictive control) for XAO which will be required for instruments such as the Planetary Camera and Spectrograph of ESOs Extremely Large Telescope. The first stage corrections are performed in simulation, with the residual wavefront error at each iteration saved. The residual wavefront errors from the first stage are then injected into the GHOST using a spatial light modulator. The second stage correction is made with a Boston Michromachines Corporation 492 actuator deformable mirror and a pyramid wavefront sensor. The GHOST RTC pipeline as implemented using COSMIC is displayed below.

![ghost_pipeline](/img/ghost-pipeline.png)

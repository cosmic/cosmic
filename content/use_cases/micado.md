---
title: MICADO
bigimg: [{src: "../../img/micado-banner.jpg",  desc: "MICADO, Credits: ESO"}]
---

 # MICADO RTC
[MICADO](https://elt.eso.org/instrument/MICADO/) is the Multi-AO Imaging Camera for Deep Observations. It will equip the [ELT](https://elt.eso.org/) with a first
light capability for diffraction limited imaging at near-infrared wavelengths. The instrument is
optimized to work with the laser guide star multi-conjugate adaptive optics (MCAO) module
developed by the [MORFEO](https://elt.eso.org/instrument/MORFEO/) consortium. It also includes a jointly developed Single-Conjugate Adaptive
Optics (SCAO) mode that uses only a single natural guide star.

The RTC, designed and built at Observatoire de Paris and relying on COSMIC, shall provide all the functionalities required for MICADO SCAO test, calibration and real-time correction of turbulence and ELT defects. The functions are implemented in the different components of the RTC.

The figure below depicts the actual implementation of the COSMIC platform made for the MICADO SCAO RTC.
The HRTC is composed of a single node which receives sensors frames from the Internal Communication Infrastructure, and publishes real-time telemetry through the same infrastructure. It is also plugged to the ELT Deterministic, Control and Time Reference Networks through the ELT Network Infrastructure, in particular to drive [M4](https://elt.eso.org/mirror/M4/) & [M5](https://elt.eso.org/mirror/M5/), the deformable mirrors embedded into the ELT design.

The SRTC is split into 4 components:
- HRTC Gateway: receives real-time telemetry from the HRTC and broadcasts it to other S-RTC components
- SRTC Gateway: receives commands from the AO Control System, monitors and controls all the processes on the SRTC (including HRTC loop supervisor) & collects metadata for archive purpose
- Storage Node: stores useful telemetry data for archive and post-processing purposes
- Computation Node: executes all the SRTC Data Tasks for AO loop optimization

![micado_archi](/img/micado-archi.png)

The HRTC architecture is the result of a bottom-up approach from the availability of the components to the performance goals and general considerations discussed above:
- GPU are hardware accelerators in a x86 server available on the PCIe bus
- Efficient data streaming between the GPU and the network controller is provided by standard NIC with DPDK support (such as Mellanox ConnectX-6 MCX621102AN).
- Telemetry and general communication should be supported by an additional standard network controller

The figure below provides details on the different datapaths in the HRTC module. 

![micado_data](/img/micado-datapaths.png)

Real-time data from the sensors / to the actuators are received / published through the [Mellanox ConnectX-6](https://www.nvidia.com/en-us/networking/ethernet-adapters/) NIC. Data are directly written on GPU shared memory to make them available for all GPU computing processes. This operation is made using DPDK which includes a library developed by NVIDIA, [gpudev](https://doc.dpdk.org/guides/prog_guide/gpudev.html), that enables the copy of packets directly in the GPU memory using GPUDirect and DPDK mbuf structure.


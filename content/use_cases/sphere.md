---
title: SPHERE+
bigimg: [{src: "../../img/sphere-banner-wide.png",  desc: "Credits: ESO"}]
---

[SPHERE+](https://sites.lesia.obspm.fr/sphereplus/) will be an upgrade of the existing VLT instrument [SPHERE](https://www.eso.org/sci/facilities/paranal/instruments/sphere.html) which has produced a huge amount of important scientific results, primarily in the field of exoplanetary system direct imaging. This upgrade aims to enhance the current capabilities of SPHERE, mainly in terms of high contrast imaging. The instrumental concept relies on a second stage adaptive optics correction in the near infrared using a pyramid WFS and a C-RED camera.
The corresponding upgrade of the RTC, SAXO+, will rely on the COSMIC platform. Following ESO standards, the SAXO+ RTC will consist of two main functional blocks:
	A Hard-real-time controller (HRTC), meeting the requirements for high throughput, low latency and low jitter and taking as input pixel data from camera and providing as output the drive data for deformable mirror 
	A Soft-real-time cluster (SRTC), providing a computing facility for the configuration, calibration and optimization of the hard-real-time pipeline (supervisor) and for data telemetry and storage (telemetry)
The figure below depicts the overall architecture of SAXO+.

![sphere_archi](/img/sphere-archi.png)

The H-RTC will be composed of a single node equipped with GPUs, while the S-RTC will be divided into 4 nodes. All those nodes are interconnected through a RTC local communication infrastructure for commands and data exchanges. 
The H-RTC will have to handle multiple HW interfaces, from 10Gb ethernet to sFPDP. As data acquisition on GPU memory is critical in the AO real-time pipeline, a specific attention has to be drawn to minimize latency and jitter. The preferred solution, already used by other instrument along with COSMIC, is to rely on a FPGA board providing the desired interfaces, and leveraging the GPUDirect technology to transport data directly from the network fabric to the GPU memory. 

The required interfaces are:
-	4x sFPDP (first stage AO)
-	1x CameraLink (Pyramid WFS)
-	2x SFP+ (second stage DM and H-RTC Gateway)
-	1x GbE (PTP)


